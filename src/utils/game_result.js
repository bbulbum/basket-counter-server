const _ = require('lodash');
const Promise = require('bluebird');
const { Op } = require('sequelize');
const Models = require('../models');

const { Teams, Members, Stats, Games } = Models;

const seasonId = 6;
const clubId = 1;
const gameDate = process.argv[2];

Promise.all([
  Teams.findAll({ where: { clubId } }),
  Members.findAll({ where: { clubId } }),
]).spread((teams, members) => {
  const teamNames = teams.reduce((ac, team) => {
    ac[team.id] = team.name;
    return ac;
  }, {});

  const memberPlus = members.reduce((ac, member) => {
    ac[member.id] = member.plus;
    return ac;
  }, {});

  return Games.findAll({
    where: {
      date: gameDate,
    },
  }).then((games) => [
    games,
    Stats.findAll({
      where: {
        gameId: {
          [Op.in]: games.map(({ id }) => id),
        },
      },
    }),
  ]).spread((games, stats) => {
    const gameScore = games.reduce((ac, game) => {
      ac[game.id] = {
        [teamNames[game.homeTeamId]] : 0,
        [teamNames[game.awayTeamId]] : 0,
      };

      return ac;
    }, {});

    stats.forEach((stat) => {
      const plus = memberPlus[stat.memberId] || 0;

      gameScore[stat.gameId][teamNames[stat.teamId]] +=
        (stat.pt3 * (3 + plus)) + (stat.pt2 * (2 + plus)) + stat.ft;
    });

    games.forEach(game => {
      gameScore[game.id][teamNames[game.homeTeamId]] += game.homePlus;
      gameScore[game.id][teamNames[game.awayTeamId]] += game.awayPlus;
    });

    return gameScore;
  });
}).then((gameResult) => {
  console.log(gameResult);
  process.exit();
});
