const _ = require('lodash');

const Models = require('../models');

const { Stats } = Models;

const date= '2019-02-10'

Stats.getWeeklyStatistic(date)
  .then((memberStats) => {
    const ptsLeader = _(memberStats).orderBy(['PTS'], ['desc']).head()
    console.log('PTS', ptsLeader.name, ptsLeader.PTS);
    const astLeader = _(memberStats).orderBy(['AST'], ['desc']).head()
    console.log('AST', astLeader.name, astLeader.AST);
    const rebLeader = _(memberStats).orderBy(['REB'], ['desc']).head()
    console.log('REB', rebLeader.name, rebLeader.REB);
    const stlLeader = _(memberStats).orderBy(['STL'], ['desc']).head()
    console.log('STL', stlLeader.name, stlLeader.STL);
    const blkLeader = _(memberStats).orderBy(['BLK'], ['desc']).head()
    console.log('BLK', blkLeader.name, blkLeader.BLK);
    const toLeader = _(memberStats).orderBy(['TO'], ['desc']).head()
    console.log('TO', toLeader.name, toLeader.TO);
    const pfLeader = _(memberStats).orderBy(['PF'], ['desc']).head()
    console.log('PF', pfLeader.name, pfLeader.PF);

    process.exit();
  });
