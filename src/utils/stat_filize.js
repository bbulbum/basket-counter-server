const Path = require('path');
const Promise = require('bluebird');
const moment = require('moment');
const XlsxPopulate = require('xlsx-populate');

const Models = require('../models');
const {
  Members, Games, Teams, Attendances, Stats
} = Models;

const gameDate = moment(process.argv[2]);
const xlsxGameDate = XlsxPopulate.dateToNumber(gameDate.toDate());

const getFilename = (date) => `NRIM_26_${date.format('MM-DD')}.xlsx`;
const FILE_PATH = Path.resolve(__dirname, `../../files/${getFilename(gameDate.clone().subtract({ weeks: 1 }))}`);

const getMemberAttribute = (date) => Attendances.findAll({
  attributes: ['point', 'isLeft'],
  include: [{
    model: Members,
    attributes: ['id', 'no', 'name'],
    as: 'member',
  }],
  where: {
    gameDate: date,
  }
})
.then(attendances => attendances.reduce((ac, attendance) => {
  const { member, point, isLeft } = attendance;
  const { no, name } = member;

  ac[`${no}_${name}`] = { point: point - isLeft, isLeft };

  return ac;
}, {}));

const getMemberStats = (date) => Stats.getWeeklyStatistic(date.format('YYYY-MM-DD'))
  .then(stats => stats.reduce((ac, item) => {
    ac[item.name] = [
      item.Q,
      item['3pt'],
      item['2pt'],
      item.FT,
      item.AST,
      item.REB,
      item.STL,
      item.BLK,
      item.TO,
      item.PF,
    ];

    return ac;
  }, {}));


Promise.all([
  XlsxPopulate.fromFileAsync(FILE_PATH),
  getMemberAttribute(gameDate),
  getMemberStats(gameDate),
])
.spread((workbook, memberAttPoints, memberStats) => {
  const range = workbook.sheet(0).usedRange();
  const noCol = 3;
  const nameCol = 4;
  let calculated = {};

  let dateCol;
  workbook.sheet(0).row(1)._cells
    .some((item) => {
      if (!item) {
        return false;
      }

      if (item.formula()) {
        let [address, number] = item.formula().split('+');
        const addressValue = calculated[address.trim()] || workbook.sheet(0).cell(address.trim()).value()
        calculated[item.address()] = Number(addressValue) + Number(number);
      }

      if((item.value() || calculated[item.address()]) == xlsxGameDate) {
        dateCol = item.columnNumber();
        return true;
      }
    });

  if (!dateCol) {
    throw new Error('NO DATE COLUMN MATCHED');
  }

  console.log('attendance checking start');

  for(let R = 2; R <= range.endCell().rowNumber(); ++R) {
    const no = workbook.sheet(0).cell(R, noCol).value();
    const name = workbook.sheet(0).cell(R, nameCol).value();

    if (!name) {
      break;
    }

    console.log(no, name);

    if (memberAttPoints[`${no}_${name}`]) {
      const { point, isLeft } = memberAttPoints[`${no}_${name}`];

      workbook.sheet(0).cell(R, dateCol).value(point);

      if (isLeft) {
        const style = workbook.sheet(0).cell(R, dateCol).style('fill', 'e2e2e2');
      }
    } else {
      workbook.sheet(0).cell(R, dateCol).value(0);
    }
  }

  console.log('attendance checking end');
  console.log('stat checking start');

  let statDateRow;
  calculated = {};

  for (let sheetIndex = 2; sheetIndex < workbook.sheets().length; sheetIndex++) {
    const memberSheet = workbook.sheet(sheetIndex);

    if (!statDateRow) {
      for (let row = 2; row < memberSheet.usedRange().endCell().rowNumber(); row++) {
        const _cell = memberSheet.cell(row, 2);

        if (_cell.formula()) {
          const [address, number] = _cell.formula().split('+');
          const addressValue = calculated[address.trim()] || memberSheet.cell(address.trim()).value();
          calculated[_cell.address()] = Number(addressValue) + Number(number);
        }

        const value = calculated[_cell.address()] || _cell.value();

        if(!value) {
          break;
        }

        if (value == xlsxGameDate) {
          statDateRow = row;
          console.log('stat date row set');
          break;
        }
      }
    }


    if (!statDateRow) {
      throw new Error('NO STAT DATE COLUMN MATCHED');
    }

    const stat = memberStats[memberSheet.name()];
    console.log(memberSheet.name(), stat);

    for(let C = 3; C <= 12; C++) {
      memberSheet.cell(statDateRow, C).value(stat ? stat[C-3] : 0);
    }
    console.log('stat write');
  }

  // Write to file.
  return workbook.toFileAsync(Path.resolve(FILE_PATH, `../${getFilename(gameDate)}`));
})
.then(() => {
  console.log('write finished');
  process.exit();
})
.catch((err) => {
  console.log('ERROR', err);
  process.exit();
});
