const Promise = require('bluebird');
const NodeCache = require('node-cache');

const Models = require('../models');

const { Games } = Models;
const mCache = new NodeCache({ stdTTL: 60 * 5, checkperiod: 60 });

const getFromCache = (key) => new Promise((resolve, reject) => {
  mCache.get(key, (err, value) => {
    if (err || value == undefined) {
      reject('NO_VALUE');
      return;
    }

    resolve(value);
  });
});

const getSeasonIdByGameId = (gameId) => getFromCache(gameId)
  .catch(() =>
    Games.findById(gameId).then(({ seasonId }) => {
      mCache.set(gameId, seasonId);
      return seasonId;
    })
  );

module.exports = {
  getSeasonIdByGameId,
};
