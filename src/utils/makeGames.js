const Promise = require('bluebird');
const Op = require('sequelize').Op;
const Models = require('../models');

const {
  Teams,
  Seasons,
  Games,
} = Models;

const N = 'N';
const R = 'R';
const I = 'I';
const M = 'M';
const seasonNo = 26;

const scheduleStrings = [
  '2019/10/6 N R I M M N R I',
  '2019/10/13 I N R M N R M I',
  '2019/10/20 I R N M M I R N',
  '2019/10/27 R M I N M I N R',
  '2019/11/3 M N I R N I R M',
  '2019/11/10 M I N R I N R M',
  '2019/11/17 N I R M I R M N',
  '2019/11/24 R I M N N R I M',
  '2019/12/1 N R M I R M I N',
  '2019/12/8 N M R I M R I N',
  '2019/12/15 I M N R M N R I',
  '2019/12/22 M R I N R I N M',
  '2019/12/29 R N M I N M I R',
];

const schedules = scheduleStrings.map((scheduleString) => {
  const splitted = scheduleString.split(' ');

  const schedule = {
    date: splitted.shift(),
    games: [],
  };

  while(splitted.length > 0) {
    const game = [splitted.shift(), splitted.shift()];
    schedule.games.push(game);
  }

  return schedule;
});

Promise.all([
  Seasons.findOne({
    attributes: ['id'],
    where: {
      season: seasonNo,
    },
  }),
  Teams.findAll({
    attributes: ['id', 'name'],
    where: {
      name: {
        [Op.in]: [N, R, I, M],
      },
    },
  }),
])
  .then(([season, teams]) => {
    const teamIds = teams.reduce((ac, team) => {
      ac[team.name] = team.id;
      return ac;
    }, {});

    const seasonId = season.id;

    return Promise.each(schedules, (schedule, scheduleIndex) => {
      const { date, games } = schedule;

      return Promise.each(games, (homeAway, index) => {
        const [homeTeam, awayTeam] = homeAway;
        const round = (scheduleIndex % 12) + 1;
        const gameSet = (Math.ceil(scheduleIndex / 2) % 6) || 6;

        return Games.create({
          game: index + 1,
          seasonId,
          round,
          gameSet,
          homeTeamId: teamIds[homeTeam],
          awayTeamId: teamIds[awayTeam],
          date: date,
        })
      })
    })
  })
  .then((result) => {
    console.log('success', result);
    process.exit();
  })
  .catch((err) => {
    console.error(err);
    process.exit();
  });
