const Promise = require('bluebird');
const moment = require('moment');
const XLSX = require('xlsx');

const Models = require('../models');

const {
  Members, Games, Teams, Attendances
} = Models;

const statFile = XLSX.readFile('../../files/NRIM_23.xlsx');
const attRange = XLSX.utils.decode_range(statFile.Sheets["출석"]["!ref"]);

const noCol = 2;
const nameCol = 3;
const dateStartCol = 4;

const attendanceSheet = statFile.Sheets["출석"];
const attendances = [];

for(let C = dateStartCol; C <= attRange.e.c; ++C) {
  const date_ref = XLSX.utils.encode_cell({c:C, r:0});
  if (attendanceSheet[date_ref].v == 'Total') {
    console.log('END OF GAME DATE');
    break;
  }

  if (!attendanceSheet[date_ref]) {
    console.log('NO GAME DATE');
    continue;
  }

  const [gameMonth, gameDate] = attendanceSheet[date_ref].w.split('/');
  const date = moment().set({ month: Number(gameMonth) - 1, date: Number(gameDate) });

  for(let R = 1; R <= attRange.e.r; ++R) {
    const no_ref = XLSX.utils.encode_cell({c:noCol, r:R});
    const name_ref = XLSX.utils.encode_cell({c:nameCol, r:R});
    const attendanceRef = XLSX.utils.encode_cell({c:C, r:R});
    if (!attendanceSheet[attendanceRef]) {
      console.log('END OF PLAYER ATTENDANCE');
      break;
    }
    attendances.push({
      date: date.format('YYYY-MM-DD'),
      no: attendanceSheet[no_ref].v,
      name: attendanceSheet[name_ref].v,
      point: attendanceSheet[attendanceRef].v,
    });
  }
}

Teams.findAll({
  attributes: ['id', 'name'],
  include: [{
    model: Members,
    attributes: ['id', 'no', 'name', 'plus'],
    as: 'members',
    through: {
      attributes: [],
      where: {
        seasonId: 1,
      },
    },
  }]
})
.then(teams => teams.reduce((ac, team) => {
  const { id: teamId, members } = team;

  members.forEach(({ id: memberId, no, name }) => {
    ac[`${no}_${name}`] = {
      memberId,
      teamId,
    }
  });

  return ac;
}, {}))
.then(members => Promise.map(attendances, (attendance) => {
  const { date, no, name, point } = attendance;
  const { memberId, teamId } = members[`${no}_${name}`];

  return Attendances.create({
    seasonId: 1,
    gameDate: date,
    memberId,
    teamId,
    point,
  })
  .catch(err => {
    console.log('ERROR CREATE', `${no}_${name}`);
    return null;
  });
}))
.then(() => {
  console.log('upload finished');
  process.exit();
});
