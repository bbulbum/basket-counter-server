const Promise = require('bluebird');
const Models = require('../models');

const {
  TeamMemberMappings,
  Teams,
  Members,
  Seasons,
} = Models;

const teamMembersArray = `
team N
0 신재영
8 유윤식
15 김유창
17 이윤휘
28 최규범
32 이효민
35 배은상
39 최상구
team R
6 이윤범
20 오중수
25 한상혁
34 김재민
44 배형진
45 정용태
69 최재웅
92 이승현
team I
3 노정태
4 박동철
5 이의양
10 임준섭
13 박종호
22 곽상준
24 이동훈
77 안병철
team M
1 이정희
11 이재훈
21 이효섭
23 이득호
27 이형철
29 이수득
37 정주헌
99 홍지현
`.split('\n');

let curTeam;

const members = teamMembersArray.reduce((ac, str) => {
  if (str.length === 0) return ac;

  const [no, name] = str.split(' ');

  if (no === 'team') {
    curTeam = name;
    return ac;
  }

  ac.push({
    teamName: curTeam,
    no,
    name,
  });

  return ac;
}, []);

const seasonNo = 25;

Seasons.findOne({
  attributes: ['id'],
  where: {
    season: seasonNo,
  },
}).then((season) => Promise.each(members, ({ teamName, no, name }) => {
    const teamPromise = Teams.findOne({
      attributes: ['id'],
      where: {
        name: teamName,
      },
    });

    const memberPromise = Members.findOne({
      attributes: ['id'],
      where: {
        name: name,
      },
    });

    return Promise.all([
      teamPromise,
      memberPromise,
      season,
    ]).then(([ team, member, season ]) => TeamMemberMappings.create({
      teamId: team.id,
      memberId: member.id,
      seasonId: season.id,
    }));
  })
).then((result) => {
  console.log('success', result);
  process.exit();
}).catch((err) => {
  console.error(err);
  process.exit();
});
