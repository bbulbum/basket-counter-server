const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Stats = sequelize.define('stats', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  gameId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  teamId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  memberId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  quarter: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  pt3: {
    type: Sequelize.INTEGER,
  },
  pt2: {
    type: Sequelize.INTEGER,
  },
  ft: {
    type: Sequelize.INTEGER,
  },
  ast: {
    type: Sequelize.INTEGER,
  },
  reb: {
    type: Sequelize.INTEGER,
  },
  stl: {
    type: Sequelize.INTEGER,
  },
  blk: {
    type: Sequelize.INTEGER,
  },
  to: {
    type: Sequelize.INTEGER,
  },
  pf: {
    type: Sequelize.INTEGER,
  },
  statCreatedAt: {
    type: Sequelize.INTEGER,
  }
}, {
  indexes: [{
    unique: true,
    fields: ["gameId", "teamId", "playerId", "quarter"],
  }]
});

Stats.removeAttribute('id');

Stats.getWeeklyStatistic = (date) => sequelize.query(`
  SELECT S.*, I.Q
  FROM (
  	SELECT
  		m.id as memberId, m.no, m.name, m.thumbnailUrl,
  		sum(s.pt3) AS 3pt, sum(s.pt2) AS 2pt, sum(s.ft) AS FT, sum(s.ast) AS AST,
    		sum(s.reb) AS REB, sum(s.stl) AS STL, sum(s.blk) AS BLK, sum(s.to) AS \`TO\`, sum(s.pf) AS PF,
    		((sum(s.pt3) * (3 + m.plus)) + (sum(s.pt2) * (2 + m.plus)) + sum(s.ft)) AS PTS
  	FROM stats s
  	LEFT JOIN members m ON m.id = s.memberId
    WHERE s.gameId IN (select id from games where date = :date)
      AND s.memberId < 99998
  	GROUP BY s.memberId
  ) S
  LEFT JOIN (
  	SELECT i.memberId, sum(case i.inout when 'IN' then 1 when 'OUT' then 0 else 0.5 end) as Q
  	FROM inouts i
  	WHERE i.gameId IN (select id from games where date = :date)
  	GROUP BY i.memberId
  ) I ON I.memberId = S.memberId;
`, { replacements: { date } })
  .then(([results]) => results)
  .then(results => results.map((row) => {
    const item = {};
    Object.keys(row).forEach((key) => {
      const value = row[key];
      item[key] = isNaN(value) ? value : Number(value);
    });
    return item;
  }));

module.exports = Stats;
