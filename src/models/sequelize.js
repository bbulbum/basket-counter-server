const Sequelize = require('sequelize');

const database = require('./database.json');

const sequelize = new Sequelize(database.database, database.user, database.pass, {
  host: database.host,
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  timezone: 'Asia/Seoul',
});

module.exports = sequelize;
