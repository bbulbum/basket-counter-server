const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Inouts = sequelize.define('inouts', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  gameId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  teamId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  memberId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  quarter: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  inout: {
    type: Sequelize.ENUM('OUT', 'IN', 'INOUT', 'OUTIN'),
  },
}, {
  indexes: [{
    unique: true,
    fields: ['gameId', 'teamId', 'playerId', 'quarter'],
  }]
});

Inouts.removeAttribute('id');

module.exports = Inouts;
