const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Seasons = sequelize.define('seasons', {
  season: {
    type: Sequelize.INTEGER,
  },
  clubId: {
    type: Sequelize.INTEGER,
  },
});

module.exports = Seasons;
