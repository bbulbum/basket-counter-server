const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Members = sequelize.define('members', {
  no: {
    type: Sequelize.INTEGER,
  },
  name: {
    type: Sequelize.STRING,
  },
  position: {
    type: Sequelize.ENUM('C', 'PF', 'SF', 'PG', 'SG'),
  },
  plus: {
    type: Sequelize.INTEGER(2),
  },
  clubId: {
    type: Sequelize.INTEGER,
  },
});

module.exports = Members;
