const Clubs = require('./clubs');
const Seasons = require('./seasons');
const Teams = require('./teams');
const Members = require('./members');
const TeamMemberMappings = require('./team_member_mappings');
const Games = require('./games');
const Stats = require('./stats');
const Inouts = require('./inouts');
const Attendances = require('./attendances');

Seasons.belongsTo(Clubs, { foreignKey: 'clubId', as: 'club' });
Clubs.hasMany(Seasons, { foreignKey: 'clubId', as: 'seasons' });

Teams.belongsTo(Clubs, { foreignKey: 'clubId', as: 'club' });
Clubs.hasMany(Teams, { foreignKey: 'clubId', as: 'teams' });

Members.belongsTo(Clubs, { foreignKey: 'clubId', as: 'club' });
Clubs.hasMany(Members, { foreignKey: 'clubId', as: 'members' });

Seasons.belongsToMany(Teams, { through: TeamMemberMappings, foreignKey: 'seasonId', as: 'teams' });

Teams.belongsToMany(Members, { through: TeamMemberMappings, foreignKey: 'teamId', as: 'members' });
Members.belongsToMany(Teams, { through: TeamMemberMappings, foreignKey: 'memberId', as: 'teams' });

Games.belongsTo(Seasons, { foreignKey: 'seasonId', as: 'season' });
Seasons.hasMany(Games, { foreignKey: 'seasonId', as: 'games' });
Games.belongsTo(Teams, { foreignKey: 'homeTeamId', as: 'homeTeam' });
Games.belongsTo(Teams, { foreignKey: 'awayTeamId', as: 'awayTeam' });
Teams.hasMany(Games, { foreignKey: 'homeTeamId', as: 'homeGames' });
Teams.hasMany(Games, { foreignKey: 'awayTeamId', as: 'awayGames' });

Games.hasMany(Stats, { foreignKey: 'gameId' });
Teams.hasMany(Stats, { foreignKey: 'teamId' });
Members.hasMany(Stats, { foreignKey: 'memberId' });

Games.hasMany(Inouts, { foreignKey: 'gameId' });
Teams.hasMany(Inouts, { foreignKey: 'teamId' });
Members.hasMany(Inouts, { foreignKey: 'memberId' });

Teams.hasMany(Attendances, { foreignKey: 'teamId' });
Members.hasMany(Attendances, { foreignKey: 'memberId' });
Attendances.belongsTo(Members, { foreignKey: 'memberId', as: 'member' });

module.exports = {
  Clubs,
  Seasons,
  Teams,
  Members,
  TeamMemberMappings,
  Games,
  Stats,
  Attendances,
  Inouts,
}
