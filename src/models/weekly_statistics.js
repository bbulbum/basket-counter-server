const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const WeeklyStatistics = sequelize.define('weekly_statistics', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  gameDate: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  teamId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  memberId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  pt3: {
    type: Sequelize.INTEGER,
  },
  pt2: {
    type: Sequelize.INTEGER,
  },
  ft: {
    type: Sequelize.INTEGER,
  },
  ast: {
    type: Sequelize.INTEGER,
  },
  reb: {
    type: Sequelize.INTEGER,
  },
  stl: {
    type: Sequelize.INTEGER,
  },
  blk: {
    type: Sequelize.INTEGER,
  },
  to: {
    type: Sequelize.INTEGER,
  },
  pf: {
    type: Sequelize.INTEGER,
  },
  pts: {
    type: Sequelize.INTEGER,
  },
});

module.exports = WeeklyStatistics;
