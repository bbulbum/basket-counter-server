const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Teams = sequelize.define('teams', {
  name: {
    type: Sequelize.STRING,
  },
  clubId: {
    type: Sequelize.INTEGER,
  }
});

module.exports = Teams;
