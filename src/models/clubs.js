const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Clubs = sequelize.define('clubs', {
  name: {
    type: Sequelize.STRING,
  },
});

module.exports = Clubs;
