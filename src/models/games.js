const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Games = sequelize.define('games', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  round: {
    type: Sequelize.INTEGER(3),
  },
  gameSet: {
    type: Sequelize.INTEGER(3),
  },
  game: {
    type: Sequelize.TINYINT,
  },
  homeTeamId: {
    type: Sequelize.INTEGER,
  },
  awayTeamId: {
    type: Sequelize.INTEGER,
  },
  homeCount: {
    type: Sequelize.INTEGER,
  },
  homePlus: {
    type: Sequelize.INTEGER,
  },
  awayCount: {
    type: Sequelize.INTEGER,
  },
  awayPlus: {
    type: Sequelize.INTEGER,
  },
  date: {
    type: Sequelize.DATEONLY,
  },
});

module.exports = Games;
