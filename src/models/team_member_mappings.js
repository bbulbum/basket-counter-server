const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const TeamMemberMappings = sequelize.define('team_member_mappings', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  teamId: {
    type: Sequelize.INTEGER,
  },
  memberId: {
    type: Sequelize.INTEGER,
  }
});

module.exports = TeamMemberMappings;
