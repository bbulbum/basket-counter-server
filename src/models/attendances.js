const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Attendances = sequelize.define('attendances', {
  seasonId: {
    type: Sequelize.INTEGER,
  },
  gameDate: {
    type: Sequelize.DATEONLY,
    primaryKey: true,
  },
  teamId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  memberId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  point: {
    type: Sequelize.INTEGER,
  },
  isLeft: {
    type: Sequelize.BOOLEAN,
  },
}, {
  indexes: [{
    unique: true,
    fields: ["gameDate", "playerId"],
  }]
});

Attendances.removeAttribute('id');

module.exports = Attendances;
