const _ = require('lodash');
const express = require('express');
const Promise = require('bluebird');
const Sequelize = require('sequelize');
const moment = require('moment');

const cache = require('../utils/cache');
const Models = require('../models');

const router = express.Router();
const { Op } = Sequelize;

const {
  Clubs, Seasons, Teams,
  Members, TeamMemberMappings, Games, Stats,
  Attendances, Inouts,
} = Models;

/* GET home page. */
router.get('/seasons', (req, res, next) => {
  Clubs.findById(1, {
    include: [{
      model: Seasons,
      as: 'seasons',
    }],
  })
  .then((club) => {
    res.send(club.seasons);
  })
  .catch(next);
});

router.get('/seasons/:seasonId/games', (req, res, next) => {
  const { seasonId } = req.params;

  Games.findAll({
    attributes: ['id', 'date', 'game', 'homeTeamId', 'awayTeamId'],
    where: {
      seasonId,
    },
  })
  .then((games) => {
    res.send(games);
  })
  .catch(next);
});

router.get('/seasons/:seasonId/teamMembers', (req, res, next) => {
  const { seasonId } = req.params;

  Teams.findAll({
    attributes: ['id', 'name'],
    include: [{
      model: Members,
      attributes: ['id', 'no', 'name', 'plus'],
      as: 'members',
      required: true,
      through: {
        model: TeamMemberMappings,
        attributes: [],
        where: {
          seasonId,
        },
      },
    }]
  })
  .then((teams) => {
    res.send(teams);
  })
  .catch(next);
});

router.get('/seasons/:seasonId/stats', (req, res, next) => {
  const { seasonId } = req.params;

  Stats.findAll({
    attributes: ['gameId', 'teamId', 'memberId', 'quarter', 'pt3', 'pt2', 'ft', 'ast', 'reb', 'stl', 'blk', 'to', 'pf', 'statCreatedAt'],
    where: {
      seasonId,
    },
  })
  .then((stats) => {
    res.send(stats);
  })
  .catch(next);
});

router.get('/seasons/:seasonId/attendances', (req, res, next) => {
  const { seasonId } = req.params;

  Attendances.findAll({
    where: {
      seasonId,
      point: {
        [Op.gt]: 0,
      },
    },
  })
  .then((attendances) => {
    res.send(attendances);
  })
  .catch(next);
});

router.get('/seasons/:seasonId/inouts', (req, res, next) => {
  const { seasonId } = req.params;

  Inouts.findAll({
    attributes: ['gameId', 'teamId', 'quarter', 'memberId', [Sequelize.literal('(`inout` + 0)'), 'inout']],
    where: {
      seasonId,
    },
  })
  .then((inouts) => {
    res.send(inouts);
  })
  .catch(next);
});

router.get('/attendances/:gameDate', (req, res, next) => {
  const { gameDate } = req.params;

  Attendances.findAll({
    where: {
      gameDate,
      point: {
        [Op.gt]: 0,
      },
    },
  })
  .then((attendances) => {
    res.send(attendances);
  })
  .catch(next);
});

router.get('/freethrows/:gameDate', (req, res, next) => {
  const { gameDate } = req.params;

  Games.findAll({
    attributes: ['id'],
    where: {
      date: gameDate,
    }
  })
  .then(games => games.map(({ id }) => id))
  .then(gameIds => Stats.findAll({
    attributes: ['gameId', 'teamId', 'memberId', 'ft'],
    where: {
      gameId: {
        [Op.in]: gameIds,
      },
      quarter: -1,
    }
  }))
  .then(stats => stats.map(stat => {
    const { gameId, teamId, memberId, ft } = stat;

    return { gameId, teamId, memberId, ft };
  }))
  .then(freethrows => {
    res.send(freethrows);
  })
  .catch(next);
});

router.get('/games/:gameId/stats/:teamId', (req, res, next) => {
  const { gameId, teamId } = req.params;

  Promise.all([
    Games.findById(gameId),
    Stats.findAll({
      attributes: ['gameId', 'teamId', 'memberId', 'quarter', 'pt3', 'pt2', 'ft', 'ast', 'reb', 'stl', 'blk', 'to', 'pf', 'statCreatedAt'],
      where: {
        gameId,
        teamId,
      },
    })
  ])
  .spread((game, stats) => {
    const { homeTeamId, homePlus, awayPlus } = game;
    const plusCount = Number(teamId) === homeTeamId ? homePlus : awayPlus;

    stats.push({
      gameId,
      teamId,
      quarter: 4,
      ft: plusCount,
    });

    return stats;
  })
  .then(stats => {
    res.send(stats);
  })
  .catch(next);
});

router.get('/games/:gameId/inouts/:teamId', (req, res, next) => {
  const { gameId, teamId } = req.params;

  Inouts.findAll({
    attributes: ['gameId', 'teamId', 'quarter', 'memberId', [Sequelize.literal('(`inout` + 0)'), 'inout']],
    where: {
      gameId,
      teamId,
    },
  })
  .then((inouts) => {
    res.send(inouts);
  })
  .catch(next);
});

router.post('/games/:gameId/stats', (req, res, next) => {
  const { gameId } = req.params;
  const { teamId, memberId, quarter, pt3, pt2, ft, ast, reb, stl, blk, to, pf, statCreatedAt } = req.body;

  cache.getSeasonIdByGameId(gameId)
    .then((seasonId) => Stats.upsert({
      seasonId, gameId, teamId, memberId, quarter,
      pt3, pt2, ft, ast, reb, stl, blk, to, pf, statCreatedAt,
    }))
    .then(() => {
      res.send({ result: 'success' });
    })
    .catch(next);
});

router.get('/games/:gameId/advantage', (req, res, next) => {
  const { teamId } = req.query;
  const { gameId } = req.params;

  Games.findById(gameId)
    .then((game) => {
      const { homeTeamId } = game;

      const plusColumn = teamId === homeTeamId ? 'homePlus' : 'awayPlus';

      res.json({
        advantage: game[plusColumn],
      });
    })
    .catch(next);
});

router.post('/games/:gameId/advantage', (req, res, next) => {
  const { teamId, count } = req.body;
  const { gameId } = req.params;

  Games.findById(gameId)
    .then((game) => {
      const { homeTeamId } = game;
      const updateColumn = Number(teamId) === homeTeamId ? 'homePlus' : 'awayPlus';

      return Games.update({
        [updateColumn]: count,
      }, {
        where: {
          id: gameId,
        },
      });
    })
    .then(() => {
      res.json({ result: 'success' });
    })
    .catch(next);
});

router.post('/attendances', (req, res, next) => {
  const { seasonId, gameDate, teamId, memberId, point, isLeft } = req.body;

  Attendances.upsert({
    seasonId,
    gameDate,
    teamId,
    memberId,
    point,
    isLeft,
  })
  .then(() => {
    res.send({ result: 'success' });
  })
  .catch(next);
});

router.post('/games/:gameId/inouts', (req, res, next) => {
  const { gameId } = req.params;
  const { teamId, quarter, memberId, inout } = req.body;

  cache.getSeasonIdByGameId(gameId)
    .then((seasonId) => Inouts.upsert({
      seasonId,
      gameId,
      teamId,
      quarter,
      memberId,
      inout,
    }))
    .then(() => {
      res.send({ result: 'success' });
    })
    .catch(next);
});

router.get('/weekly_leader/:date', (req, res, next) => {
  const { date } = req.params;

  Stats.getWeeklyStatistic(date)
    .then((memberStats) => {
      const leaders = ['PTS', 'AST', 'REB', 'STL', 'BLK', 'TO', 'PF'].map((statName) => {
        const ordered = _.orderBy(memberStats, [statName], ['desc']);
        const members = [];
        let leaderScore = -1;
        ordered.some((member) => {
          const { name, [statName]: score, thumbnailUrl } = member;
          if (leaderScore > score) {
            return true;
          }
          leaderScore = score;
          members.push({ name, thumbnailUrl });
          return false;
        });

        return {
          statName,
          leaderScore,
          members,
        }
      });

      console.log('score', leaders);

      res.render('weeklyLeader', { leaders });
    })
    .catch(next);
});

module.exports = router;
