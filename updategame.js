const Promise = require('bluebird');
const sequelize = require('./src/models/sequelize');

const games = [
  '2018-11-11 R I M N N R I M',
  '2018-11-18 M R I N R I N M',
  '2018-11-25 N R M I R M I N',
  '2018-12-02 I R N M M I R N',
  '2018-12-09 N I R M I R M N',
  '2018-12-16 M I N R I N R M',
];

const params = games.reduce((ac, game) => {
  const [date, g1h, g1a, g2h, g2a, g3h, g3a, g4h, g4a] = game.split(' ');
  ac.push([g1h, g1a, date, 1]);
  ac.push([g2h, g2a, date, 2]);
  ac.push([g3h, g3a, date, 3]);
  ac.push([g4h, g4a, date, 4]);
  return ac;
}, []);

Promise.map(params, (param) => sequelize.query(`
  update games set
    homeTeamId = (select id from teams where name = ?),
    awayTeamId = (select id from teams where name = ?)
  where \`date\` = ? and game = ?;`, { replacements: param }))
.then(() => {
  console.log('done');
});
