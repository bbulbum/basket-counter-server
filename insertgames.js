const Promise = require('bluebird');
const sequelize = require('./src/models/sequelize');

const games = [
  '2018-12-16 M I N R I N R M',
];

const params = games.reduce((ac, game) => {
  const [date, g1h, g1a, g2h, g2a, g3h, g3a, g4h, g4a] = game.split(' ');
  ac.push([g1h, g1a, date, 1]);
  ac.push([g2h, g2a, date, 2]);
  ac.push([g3h, g3a, date, 3]);
  ac.push([g4h, g4a, date, 4]);
  return ac;
}, []);

Promise.map(params, (param) => sequelize.query(`
  insert into games (seasonId, round, gameSet, homeTeamId, awayTeamId, \`date\`, game) values (
    1, 0, 0,
    (select id from teams where name = ?), (select id from teams where name = ?), ?, ?);`, { replacements: param }))
.then(() => {
  console.log('done');
});
